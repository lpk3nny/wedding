import Vue from 'vue';
import App from './App.vue';
import ApolloClient from 'apollo-boost';
import VueApollo from 'vue-apollo';
import ElementUI from 'element-ui';
import smoothscroll from 'smoothscroll-polyfill';
import 'element-ui/lib/theme-chalk/index.css';

smoothscroll.polyfill();
const apolloClient = new ApolloClient({
  uri: process.env.VUE_APP_GRAPHQL
});
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

Vue.config.productionTip = false;

Vue.use(VueApollo);
Vue.use(ElementUI);

new Vue({
  apolloProvider,
  render: h => h(App),
}).$mount('#app');
