package main

import (
	"github.com/go-bongo/bongo"
	"github.com/labstack/echo/middleware"
	"net/http"

	"github.com/labstack/echo"
)

type Person struct {
	bongo.DocumentBase `bson:",inline"`
	FirstName          string
	LastName           string
	Gender             string
	HomeAddress        struct {
		Street string
		Suite  string
		City   string
		State  string
		Zip    string
	}
}

func main() {
	config := &bongo.Config{
		ConnectionString: "localhost",
		Database:         "bongotest",
	}

	connection, err := bongo.Connect(config)

	myPerson := &Person{
		FirstName: "Testy",
		LastName:  "McGee",
		Gender:    "male",
	}

	if err != nil {
		print("mongo connection error")
	}

	saveErr := connection.Collection("people").Save(myPerson)

	if saveErr != nil {
		print("mongo save error")
	}

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "asdasd")
	})
	e.Logger.Fatal(e.Start(":1323"))
}
