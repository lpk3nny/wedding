const Server = require('koa');
const mongoose = require('mongoose');
const mount = require('koa-mount');
const graphqlHTTP = require('koa-graphql');
const schema = require('./graphql/schema');
const root = require('./graphql/root');
const cors = require( `@koa/cors` );
const logger = require('koa-logger')
const fs = require('fs')
const serve = require('koa-static');

const PORT = 3061;

mongoose.Promise = global.Promise;
mongoose.connect(
  'mongodb+srv://lpk3nny:here6FY1@cluster0-hiz8v.azure.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true }
);
mongoose.set("debug", (collectionName, method, query, doc) => {
  // console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});
const db = mongoose.connection;


const init = async () => {
  const app = new Server();

  app.use(cors());
  app.use(logger());
  app.use(serve('dist'));
  app.use(serve('public'));

  app.use(mount('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })));

  app.use(async ctx => {
    ctx.type = 'html';
    ctx.body = fs.createReadStream('./dist/index.html');
  });

  app.listen(PORT);
  // there is emoji in a string
  console.log(`🦄 Koa.js server started on ${PORT}`);
};

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', init);




