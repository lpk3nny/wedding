const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: String,
  personCount: Number,
  url: String,
  pollResults: Object,
  ownCar: Boolean,
  wayHome: String,
  participation: Boolean,
  drinks: String,
  food: String,
});

module.exports = mongoose.model('User', UserSchema);
