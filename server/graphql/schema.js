const { buildSchema } = require('graphql');

const schema = `
  type User {
    id: ID,
    name: String,
    personCount: Int,
    url: String,
    ownCar: Boolean,
    participation: Boolean,
    food: String,
    drinks: String,
    wayHome: String,
  }
  type Query {
    user(url: String): User
  }
  type Mutation {
    createUser(name: String!, personCount: Int, url: String): User
    patchUser(id: ID, name: String, personCount: Int, url: String, ownCar: Boolean, food: String, drinks: String, wayHome: String, 
    participation: Boolean, ): User
  }
`;

module.exports = buildSchema(schema);
