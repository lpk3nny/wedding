const User = require('../models/user');

module.exports = {
  listUsers() {
    return User.find();
  },
  user({url}) {
    return User.findOne({ url });
  },
  createUser(input) {
    const user = new User(input);
    return user.save();
  },
  async patchUser({id, ...input}) {
    const user = await User.findById(id);
    const res = await user.updateOne(input);
    return User.findById(id);
  },
};
